package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class KnightAdventurer extends Adventurer {

        public KnightAdventurer(Guild guild) {
                this.name = "Knight";
                //ToDo: Complete Me
                this.guild = guild;
                guild.add(this);
        }

        //ToDo: Complete Me

        @Override
        public void update() {
                Quest quest = this.guild.getQuest();
                String questType = quest.getType();
                if(questType.equals("D") || questType.equals("R") || questType.equals("E")){
                        this.getQuests().add(this.guild.getQuest());
                }
        }
}
