package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class AgileAdventurer extends Adventurer {

        public AgileAdventurer(Guild guild) {
                this.name = "Agile";
                //ToDo: Complete Me
                this.guild = guild;
                guild.add(this);
        }

        //ToDo: Complete Me

        @Override
        public void update() {
                String questType = this.guild.getQuestType();
                if(questType.equals("D") || questType.equals("R")){
                        this.getQuests().add(this.guild.getQuest());
                }
        }
}
