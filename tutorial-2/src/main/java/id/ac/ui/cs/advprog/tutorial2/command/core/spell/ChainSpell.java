package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.ArrayList;

public class ChainSpell implements Spell {
    // TODO: Complete Me
    private ArrayList<Spell> spellChain;

    public ChainSpell(ArrayList<Spell> spellChain){
        this.spellChain = spellChain;
    }

    @Override
    public void cast() {
        // TODO
        for (Spell spell : spellChain){
            spell.cast();
        }
    }

    @Override
    public void undo() {
        // TODO
        for (int ii = spellChain.size()-1; ii >= 0; ii--){
            spellChain.get(ii).undo();
        }
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }
}
